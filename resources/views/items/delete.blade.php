@extends('layouts.app')

@section('title', 'Delete Item')

@section('content')
    <h1>Delete Item</h1>
    <p>Are you sure you want to delete the item "{{ $item->name }}"?</p>
    <form action="{{ url('/items/' . $item->id) }}" method="POST">
        @csrf
        @method('DELETE')
        <button type="submit">Yes, Delete</button>
        <a href="{{ url('/items') }}">No, Cancel</a>
    </form>
@endsection
