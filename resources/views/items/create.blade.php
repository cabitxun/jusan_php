@extends('layouts.app')

@section('title', 'Create Item')

@section('content')
    <h1>Create New Item</h1>
    <form action="{{ url('/items') }}" method="POST">
        @csrf
        <label for="name">Name:</label>
        <input type="text" id="name" name="name">
        <button type="submit">Create</button>
    </form>
@endsection
