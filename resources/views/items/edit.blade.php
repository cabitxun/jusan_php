@extends('layouts.app')

@section('title', 'Edit Item')

@section('content')
    <h1>Edit Item</h1>
    <form action="{{ url('/items/' . $item->id) }}" method="POST">
        @csrf
        @method('PUT')
        <label for="name">Name:</label>
        <input type="text" id="name" name="name" value="{{ $item->name }}">
        <button type="submit">Update</button>
    </form>
@endsection
