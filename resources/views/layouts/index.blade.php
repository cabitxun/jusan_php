@extends('layouts.app')

@section('title', 'Items List')

@section('content')
    <h1>Items List</h1>
    <a href="{{ url('/items/create') }}">Create New Item</a>
    <ul>
        @foreach($items as $item)
            <li>{{ $item['name'] }}</li>
        @endforeach
    </ul>
@endsection
