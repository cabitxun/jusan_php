<?php

use App\Http\Controllers\ItemController;
use App\Models\Student;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('layouts.welcome');
});

Route::get('/hello', \App\Http\Controllers\ExampleController::class . "@viewHello");

Route::get('/index', \App\Http\Controllers\ExampleController::class . "@viewIndex");

Route::resource('items', ItemController::class);

