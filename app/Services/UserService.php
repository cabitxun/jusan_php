<?php

namespace App\Services;

use App\Repositories\IUserRepository;

class UserService
{
    public IUserRepository $userRepository;

    public function __construct(IUserRepository $repository) {
        $this->userRepository = $repository;
    }

    public function all() {
        return $this->userRepository->all();
    }

    public function find($id) {
        return $this->userRepository->find($id);
    }
    public function create(array $data) {
        return $this->userRepository->create($data);
    }

    public function store(array $data) {
        if (empty($data['id'])) {
            $users = $this->userRepository->create($data);
        } else {
            $users = $this->userRepository->update($data, $data['id']);
        }
        return $users;
    }

    public function update(array $data, $id) {
        return $this->userRepository->update($data, $id);
    }

    public function delete($id) {
        return $this->userRepository->delete($id);
    }
}
