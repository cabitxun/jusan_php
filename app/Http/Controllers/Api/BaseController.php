<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;


/**
 * @OA\Info(
 *      version="1.0.0",
 *      title="Lecture-App Swagger API",
 *      description="Lecture-App Swagger OpenApi description",
 *      @OA\Contact(
 *          email="your-email@example.com"
 *      ),
 *      @OA\License(
 *          name="Apache 2.0",
 *          url="http://www.apache.org/licenses/LICENSE-2.0.html"
 *      )
 * )
 */

class BaseController extends Controller
{

}
