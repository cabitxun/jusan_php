<?php

namespace App\Http\Controllers\Api;

use App\Exports\UsersExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\Services\UserService;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use App\Models\User;
use Maatwebsite\Excel\Facades\Excel;


class UserController extends Controller
{
    protected $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * @OA\Get(
     *     path="/api/users",
     *     @OA\Response(response="200", description="Display a listing of users.")
     * )
     */


    public function index()
    {
        return response()->json($this->userService->all());
    }


    /**
     * @OA\Post(
     *     path="/api/users",
     *     summary="Create a new user",
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/User")
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="User created",
     *         @OA\JsonContent(ref="#/components/schemas/User")
     *     )
     * )
     */

    public function store(UserRequest $request)
    {
        return $this->userService->store($request->all());
    }

    /**
     * @OA\GET(
     *     path="/api/users/{id}",
     *     summary="Find the user",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         @OA\Schema(type="integer"),
     *         description="User ID"
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="User finded",
     *         @OA\JsonContent(ref="#/components/schemas/User")
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="User didn't finded",
     *     )
     * )
     */

    public function show($id)
    {
        return $this->userService->find($id);
    }


    /**
     * @OA\PUT(
     *     path="/api/users/{id}",
     *     summary="Update the user",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         @OA\Schema(type="integer"),
     *         description="User ID"
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/User")
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="User updated",
     *         @OA\JsonContent(ref="#/components/schemas/User")
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="User didn't updated",
     *     )
     * )
     */

    public function update(UserRequest $request, $id)
    {
        return $this->userService->update($request->all(), $id);
    }


    /**
     * @OA\DELETE(
     *     path="/api/users/{id}",
     *     summary="Delete the user",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         @OA\Schema(type="integer"),
     *         description="User ID"
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="User deleted",
     *         @OA\JsonContent(ref="#/components/schemas/User")
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="User didn't deleted",
     *     )
     * )
     */

    public function destroy($id)
    {
        $this->userService->delete($id);

        return response()->json(['message' => 'Article deleted successfully']);
    }

    public function export()
    {
        return Excel::download(new UsersExport, 'users.xlsx');
    }
}
