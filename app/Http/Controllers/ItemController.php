<?php

namespace App\Http\Controllers;

use App\Facades\Parser;
use Illuminate\Http\Request;
use App\Models\Item;

class ItemController extends Controller
{
    public function index()
    {

        $items = Item::all();
        return view('items.index', compact('items'));
    }

    public function create()
    {
        return view('items.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required'
        ]);

        Item::create($request->all());

        return redirect('/items');
    }

    public function edit($id)
    {
        $item = Item::findOrFail($id);
        return view('items.edit', compact('item'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required'
        ]);

        $item = Item::findOrFail($id);
        $item->update($request->all());

        return redirect('/items');
    }

    public function destroy($id)
    {
        $item = Item::findOrFail($id);
        return view('items.delete', compact('item'));
    }

    public function delete(Request $request, $id)
    {
        $item = Item::findOrFail($id);
        $item->delete();

        return redirect('/items');
    }
}

