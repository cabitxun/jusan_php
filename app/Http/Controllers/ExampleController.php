<?php

namespace App\Http\Controllers;

use App\Facades\Parser;
use App\Models\Student;
use App\Models\User;

class ExampleController extends Controller
{
    public function viewHello() {
        $students = User::all();

        $boold = False;

        return view('layouts.hello', compact('students', 'boold'));
    }

    public function viewIndex() {
        dd(Parser::getUrl("https://www.google.kz/"));
        $items = Student::all();
        return view('layouts.index', compact('items'));
    }
}
