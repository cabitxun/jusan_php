<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $fillable = [
        'address',
        'country',
        'telephone',
        'user_id',
    ];

    public function user() {
        return $this->belongsTo(User::class);
    }

    protected $table = 'profiles';
}
