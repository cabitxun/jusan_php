<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    public $id;
    public $name;
    public $age;

    protected $table = "items";
}
