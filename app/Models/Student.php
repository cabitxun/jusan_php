<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *     schema="Student",
 *     type="object",
 *     title="Student",
 *     required={"name", "age", "email", "phone"},
 *     @OA\Property(
 *         property="id",
 *         type="integer",
 *         format="int64",
 *         description="ID"
 *     ),
 *     @OA\Property(
 *         property="name",
 *         type="string",
 *         description="Name"
 *     ),
 *     @OA\Property(
 *         property="age",
 *         type="integer",
 *         format="int64",
 *         description="Age"
 *     ),
 *     @OA\Property(
 *         property="email",
 *         type="string",
 *         format="email",
 *         description="Email"
 *     ),
 *     @OA\Property(
 *         property="phone",
 *         type="string",
 *         format="phone",
 *         description="Phone"
 *     )
 * )
 */

class Student extends Model
{
    public $id;
    public $name;
    public $age;
    public $email;
    public $phone;

    protected $table = "students";
}
