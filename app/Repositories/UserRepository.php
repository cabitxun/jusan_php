<?php

namespace App\Repositories;

use App\Models\Student;
use App\Models\User;

class UserRepository extends BaseRepository implements IUserRepository
{

    public function all()
    {
        return User::all();
    }

    public function find($id)
    {
        return User::find($id);
    }

    public function create(array $data)
    {
        return User::create(
            [
                'name' => $data['name'],
                'email' => $data['email'],
                'password' => $data['password'],
            ]
        );
    }

    public function update(array $data, $id)
    {
        return User::find($id)->update(
            [
                'name' => $data['name'],
                'email' => $data['email'],
                'password' => $data['password'],
            ]
        );
    }

    public function delete($id)
    {
        return User::find($id)->delete();
    }
}
